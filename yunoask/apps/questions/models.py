from django.db import models

from yunoask.apps.questions.managers import QuestionCommentManager

class Question(models.Model):
	title    = models.CharField("The question itself", max_length=140)
	details  = models.TextField("Details to enrich the question")

	votes    = models.IntegerField("Total amount of votes", default=0)
	views    = models.IntegerField("Total amount of views", default=0)

	author   = models.CharField("Author's username", max_length=30)
	pub_date = models.DateTimeField("Publication date", auto_now_add=True)
	up_date  = models.DateTimeField("Update date", auto_now=True)


class QuestionComment(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE)

	content  = models.TextField("Content of the comment")

	votes    = models.IntegerField("Total amount of votes", default=0)

	author   = models.CharField("Author's username", max_length=30)
	pub_date = models.DateTimeField("Publication date", auto_now_add=True)
	up_date  = models.DateTimeField("Update date", auto_now=True)

	objects  = QuestionCommentManager()